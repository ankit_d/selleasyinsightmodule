package com.insights.source;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.EventDeliveryException;
import org.apache.flume.EventDrivenSource;
import org.apache.flume.PollableSource;
import org.apache.flume.PollableSource.Status;
import org.apache.flume.conf.Configurable;
import org.apache.flume.event.EventBuilder;
import org.apache.flume.source.AbstractSource;
import org.apache.flume.source.http.HTTPBadRequestException;
import org.apache.flume.source.http.HTTPSourceHandler;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.ipc.Server.Connection;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import twitter4j.auth.AccessToken;

import com.sforce.ws.ConnectorConfig;

public class InsightSource extends AbstractSource implements 
Configurable,PollableSource,EventDrivenSource {
	static Configuration conf;
	static String hdfspath;
	static String url;
	static String accesstoken;
	static String path;
	static String Companyname;
	static String Compwebsite;
	static String ticker;
	static String chunk;
	static int refreshInterval ;
  	volatile long lastpoll=0;
	/*public final static void main(String[] args) {
		 
	    
	    
	    HttpClient httpClient = new DefaultHttpClient();
	    try {
	     
	      HttpGet httpGetRequest = new HttpGet("https://api.insideview.com/api/v1/companies?name="+Companyname+"&website="+Compwebsite+"&ticker="+ticker);
	      httpGetRequest.setHeader("accesstoken","hIeTYMCJP6eR+4ye62dMEhHciqmGvv6U9RLumqdTe5CPdRpYs9r37J1fW8aOURMHldpGEFeDc8v3Ol2fs3hylW2EkZZRbx8u4oqws6qeQEqzniTFLi7lH1cphjdBe6uX1GaCWiODH81cB3EaC96sbcmQXIDRg9MxOvaFqiH74r0=.eyJmZWF0dXJlcyI6Int9IiwiY2xpZW50SWQiOiJzcXA3aG9jMHNwZDMzYmk3c3R2ZSIsImdyYW50VHlwZSI6ImNyZWQiLCJ0dGwiOiIzMTAwMDAwMCIsImlhdCI6IjE0NDIyMDk2NjAiLCJqdGkiOiI4MjgzY2MzNC01NGRjLTQzZWUtODYzNi02YWI4ODlhN2IwMjAifQ==");
	      httpGetRequest.setHeader("Accept","application/json");
	      // Execute HTTP request
	      HttpResponse httpResponse = httpClient.execute(httpGetRequest);
	 
	      System.out.println("----------------------------------------");
	      System.out.println(httpResponse.getStatusLine());
	      System.out.println("----------------------------------------");
	 
	      // Get hold of the response entity
	      HttpEntity entity = httpResponse.getEntity();
	 
	      // If the response does not enclose an entity, there is no need
	      // to bother about connection release
	      byte[] buffer = new byte[1024];
	      if (entity != null) {
	        InputStream inputStream = entity.getContent();
	        try {
	          int bytesRead = 0;
	          BufferedInputStream bis = new BufferedInputStream(inputStream);
	          while ((bytesRead = bis.read(buffer)) != -1) {
	            String chunk = new String(buffer, 0, bytesRead);
	            System.out.println(chunk);
	          }
	        } catch (IOException ioException) {
	          // In case of an IOException the connection will be released
	          // back to the connection manager automatically
	          ioException.printStackTrace();
	        } catch (RuntimeException runtimeException) {
	          // In case of an unexpected exception you may want to abort
	          // the HTTP request in order to shut down the underlying
	          // connection immediately.
	          httpGetRequest.abort();
	          runtimeException.printStackTrace();
	        } finally {
	          // Closing the input stream will trigger connection release
	          try {
	            inputStream.close();
	          } catch (Exception ignore) {
	          }
	        }
	      }
	    } catch (ClientProtocolException e) {
	      // thrown by httpClient.execute(httpGetRequest)
	      e.printStackTrace();
	    } catch (IOException e) {
	      // thrown by entity.getContent();
	      e.printStackTrace();
	    } finally {
	      // When HttpClient instance is no longer needed,
	      // shut down the connection manager to ensure
	      // immediate deallocation of all system resources
	      httpClient.getConnectionManager().shutdown();
	    }
	}*/
	
	@Override
	public void configure(Context context) {
		// TODO Auto-generated method stub
		url=context.getString("url");
		hdfspath=context.getString("path");
		accesstoken= context.getString("accessToken");
		Companyname=context.getString("companyname");
		Compwebsite=context.getString("Compwebsite");
		ticker=context.getString("ticker");
		conf=new Configuration(); 
		
	}	
	
	@Override
	public Status process() throws EventDeliveryException {
		// TODO Auto-generated method stub
	if(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()-lastpoll)>refreshInterval){	
		HttpGet httpGetRequest= new HttpGet(url+"?name="+Companyname+"&website="+Compwebsite+"&ticker="+ticker);
		httpGetRequest.setHeader("accesstoken",accesstoken);
		httpGetRequest.setHeader("Accept","application/json");
		HttpClient httpClient = new DefaultHttpClient();
		try {
			HttpResponse httpResponse = httpClient.execute(httpGetRequest);
			HttpEntity entity = httpResponse.getEntity();
			
			byte[] buffer = new byte[100024];
			if (entity != null){
				InputStream inputStream = entity.getContent();
				
				BufferedInputStream bis = new BufferedInputStream(inputStream);
				//
				
				
				/*System.out.println("---------------------------------------------------");
				System.out.println(chunk);
				System.out.println("---------------------------------------------------");*/
				 
				 
				int bytesRead = 0;
				 
					
					//FSDataOutputStream out = hdfs.create(path);	
					
				 while ((bytesRead = bis.read(buffer)) != -1){
					 
					 chunk = new String(buffer, 0, bytesRead);
						//out.write(buffer, 0, bytesRead);
						
				 }
				/* ObjectMapper objectMapper=new ObjectMapper();
					objectMapper.setSerializationInclusion(Inclusion.NON_NULL);
				 String json = objectMapper.writeValueAsString(chunk );*/
				 
				 URI uri = new URI(hdfspath);
					// Get the instance of the HDFS
					FileSystem hdfs = FileSystem.get(uri,conf);
					
					Path path = new Path(uri);
					//hdfs.delete(path, true);
				 org.apache.flume.Event event = EventBuilder.withBody(chunk.getBytes());
				 getChannelProcessor().processEvent(event);
				 
					event=null;
					chunk=null;
				 //json=null;
				 inputStream.close();
				 
				    hdfs.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			httpGetRequest.abort();

			e.printStackTrace();
			return Status.BACKOFF;
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Status.BACKOFF;
		}
		
		
	}
		return Status.READY;
	}


/*	@Override
	public List<Event> getEvents(HttpServletRequest arg0)
			throws HTTPBadRequestException, Exception {
		// TODO Auto-generated method stub
		return null;
	}*/
	

}
